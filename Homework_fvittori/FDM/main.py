import numpy as np
import torch
import torch.optim as optim

import Classical_FDM.consts as consts 
import utils
import solvers


alpha = 0.1

#create grid 
#Discretize the domain [-0.5 , 0.5] x [-0.5 , 0.5]
x = np.arange(consts.x_min, consts.x_max+consts.dx,consts.dx)
y = np.arange(consts.y_min, consts.y_max+consts.dy,consts.dy)
# #Discretize the time domain
t = np.arange(0,consts.t_max+consts.dt, consts.dt)
# #get sizes for utility
nx , ny, nt = len(x), len(y), len(t)


        
def cn():
    #init domain
    u = utils.init_domain(nx,ny,nt)
    print("Domain Initialized Successfully!")
    #solve
    u = solvers.crank_nicolson(u,alpha,nx,ny,nt)
    print("Solution computed Successfully")
    utils.visualize_solution(u,x,y,t)


def imp_fd():
    #init domain
    u = utils.init_domain(nx,ny,nt)
    print("Domain Initialized Successfully!")
    #solve
    u = solvers.imp_FD(u,alpha,nx,ny,nt)
    print("Solution computed Successfully")

    utils.visualize_solution(u,x,y,t)


def exp_fd():
    #init domain
    u = utils.init_domain(nx,ny,nt)
    print("Domain Initialized Successfully!")
    #solve
    u = solvers.exp_FD(u,alpha,nx,ny,nt)
    print("Solution computed Successfully")

    utils.visualize_solution(u,x,y,t)

















# def inverse_problem():
#     # Given data
#     t_data = torch.tensor([0, 1, 2, 4], dtype=torch.float32)
#     u_data = torch.tensor([0, 0.0, 0.35, 12.23], dtype=torch.float32)
    
#     # Initialize alpha
#     alpha = torch.tensor(0.1, dtype=torch.float32, requires_grad=True)

#     # Set up the optimizer
#     optimizer = optim.SGD([alpha], lr=0.1)
   
#     # Training loop
#     num_epochs = 5
#     for epoch in range(num_epochs):
#         # Initialize the domain
#         u = utils.init_domain(nx, ny, nt)

#         # Solve the heat equation using exp_FD
#         u = solvers.ImpFDFunction.apply(u, alpha, nx, ny, nt)

#         # Extract the solution at the probe location
#         u_probe = u[t_data.long(), nx // 2 , ny // 2 ] # (0,0) should be in the middle

#         # Calculate the loss
#         loss = torch.sum((u_probe - u_data)**2)

#         # Perform optimization
#         optimizer.zero_grad()
#         loss.backward()




#         alpha.data = torch.clamp(alpha - optimizer.param_groups[0]['lr'] * alpha.grad, 0, 1)
#         optimizer.step()
#         optimizer.step()
#         # Print progress
       
#         print(f'Epoch: {epoch + 1}, Loss: {loss.item()}, Alpha: {alpha.item()}')




























# u = utils.init_domain(10,10,10)
# # Define value of alpha
# alpha = torch.tensor([0.1], requires_grad=True)

# # Compute output tensor and gradient with respect to alpha
# out = solvers.exp_FD_alpha(u, alpha, 10,10,10)
# out.sum().backward()
# grad = alpha.grad

# print("Gradient:", grad.item())

# def loss_fn(alpha, u):
#     # Predictions using imp_FD with current value of alpha
#     pred = solvers.imp_FD(u, alpha, 100,100,100)
#     # Observed data
#     # Given data
#     t_data = torch.tensor([0, 1, 2, 4], dtype=torch.float32)
#     u_data = torch.tensor([0, 0.0, 0.35, 12.23], dtype=torch.float32)

#     # Extract predicted values at time step t
#     u_probe = u[t_data.long(), nx // 2 , ny // 2 ]
    
#      # Calculate the loss
#     loss = torch.mean((u_probe - u_data)**2)
#     return loss


# u = utils.init_domain(100,100,100)

# # Initialize alpha
# alpha = torch.tensor([1.], requires_grad=True)

# # Initialize optimizer
# optimizer = optim.SGD([alpha], lr=0.01)

# # Train loop
# for i in range(1000):
#     # Zero gradients
#     optimizer.zero_grad()
    
#     # Compute loss and gradients
#     loss = loss_fn(alpha, u)
#     loss.backward()
    
#     # Update alpha
#     optimizer.step()
    
#     # Print progress
#     if i % 100 == 0:
#         print(f"Epoch {i}, Loss {loss.item()}, Alpha {alpha.item()}")

















