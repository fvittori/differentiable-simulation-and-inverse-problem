import torch
import torch.nn as nn
import torch.optim as optim

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


# Constants
t_min, t_max = 0, 10
x_min, x_max = -0.5, 0.5
y_min, y_max = -0.5, 0.5
Nx, Ny, Nt = 30,30,30

# Define the neural network
class HeatEquationNN_V1(nn.Module):
    def __init__(self):
        super(HeatEquationNN, self).__init__()
         
        # self.layers = nn.Sequential(
        #         nn.Linear(3,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,100),
        #         nn.ReLU(),
        #         nn.Linear(100,1))
        self.fc1 = nn.Linear(3, 50)
        self.fc2 = nn.Linear(50, 50)
        self.fc3 = nn.Linear(50, 1)
        self.lambdas = [1, 1, 1, 1, 1]

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x




        
    # def forward(self, x):
    #     return self.layers(x)

    
        
    def L_r(self, xyt):
        u_pred = self(xyt)
        grads = torch.autograd.grad(outputs=u_pred.sum(), inputs=params, create_graph=True)[0]
        u_t = grads[:, 2]
        u_x = grads[:, 0]
        u_y = grads[:, 1]

        grads2 = torch.autograd.grad(outputs=u_x.sum(), inputs=params, create_graph=True)[0]
        u_xx = grads2[:, 0]
        grads2 = torch.autograd.grad(outputs=u_y.sum(), inputs=params, create_graph=True)[0]
        u_yy = grads2[:, 1]

        alpha = 0.01
        f = u_t - alpha * (u_xx + u_yy)
        return torch.mean(f ** 2)
    



    def pinn_lr_annealing(self, L_r, L_i, params, S=1000, eta=1e-3, alpha=0.1):
        M = len(L_i)
        lambda_i = torch.ones(M)

        for n in range(S):
            lambda_i_hat = torch.zeros(M)
            grad_L_r = torch.autograd.grad(L_r(params), params, create_graph=True)[0]
            for i in range(M):
                grad_L_i = torch.autograd.grad(L_i[i](params), params, create_graph=True)[0]
                lambda_i_hat[i] = torch.max(torch.abs(grad_L_r)) / torch.mean(torch.abs(grad_L_i))

            lambda_i = (1 - alpha) * lambda_i + alpha * lambda_i_hat

            grad_L_r = torch.autograd.grad(L_r(params), params)[0]
            grad_L_i_sum = torch.zeros_like(params)
            for i in range(M):
                grad_L_i = torch.autograd.grad(L_i[i](params), params)[0]
                grad_L_i_sum += lambda_i[i] * grad_L_i

            params = params - eta * (grad_L_r + grad_L_i_sum)

        return params
    
    def visualize(self):

        x_values = torch.linspace(x_min, x_max, Nx).view(-1, 1)
        y_values = torch.linspace(y_min, y_max, Ny).view(-1, 1)
        t_values = torch.linspace(t_min, t_max, Nt).view(-1, 1)

        x = np.arange(x_min, x_max, Nx)
        y = np.arange(y_min, y_max, Ny)
        t = np.arange(0, t_max, Nt)

      

        xyt = torch.tensor([[x, y, t] for x in x_values for y in y_values for t in t_values], requires_grad=True)


        heat = self.forward(xyt).view(Nt, Nx, Ny).cpu().detach().numpy()
        fig, ax = plt.subplots()
        cax = ax.imshow(heat[0], cmap='jet', origin='lower',
                        extent=[x_min,x_max,y_min,y_max], vmin=0, vmax=1)
        cb = fig.colorbar(cax)
        def update(frame):
            ax.set_title(f"Time: {t_values[frame,0]:.2f}s")
            cax.set_array(heat[frame].reshape(Nx,Ny))
            return cax,
        ani = animation.FuncAnimation(fig, update, frames=range(1, Nt), interval=100, blit=True)
        plt.show()





def plot_loss(loss_values, epoch_values):
    plt.plot(epoch_values, loss_values)
    plt.title('Loss vs. Epochs')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.savefig("Loss.png", dpi=300, bbox_inches='tight')





if __name__ == '__main__':
    # Training data
    print("Initialializing Data")
    x_values = torch.linspace(x_min, x_max, Nx).view(-1, 1)
    y_values = torch.linspace(y_min, y_max, Ny).view(-1, 1)
    t_values = torch.linspace(t_min, t_max, Nt).view(-1, 1)

    
 
    print("Imposing Boundary Conditions")
    xyt = torch.tensor([[x, y, t] for x in x_values for y in y_values for t in t_values], requires_grad=True)
    left_boundary = torch.tensor([[-0.5, y, t] for y in y_values for t in t_values], requires_grad=True)
    right_boundary = torch.tensor([[0.5, y, t] for y in y_values for t in t_values], requires_grad=True)
    top_boundary = torch.tensor([[x, 0.5, t] for x in x_values for t in t_values], requires_grad=True)
    bottom_boundary = torch.tensor([[x, -0.5, t] for x in x_values for t in t_values], requires_grad=True)

    print("Imposing Initial Conditions")
    left_init = torch.tensor([[-0.5, y, 0] for y in y_values], requires_grad=True)
    right_init = torch.tensor([[0.5, y, 0] for y in y_values], requires_grad=True)
    top_init = torch.tensor([[x, 0.5, 0] for x in x_values ], requires_grad=True)
    bottom_init = torch.tensor([[x, -0.5, 0] for x in x_values], requires_grad=True)
    

    middle_init_values = ([x, y, 0] for x in x_values.squeeze()[1:] for y in y_values.squeeze()[1:])
    middle_init = torch.tensor(list(middle_init_values), dtype=torch.float, requires_grad=True)

    # Network, optimizer, and loss function
    print("Defining the NN")
    net = HeatEquationNN()
   
####-lel
    L_i = [
        lambda params: torch.mean((net(params) - 1.0) ** 2),
        lambda params: torch.mean(net(params) ** 2),
        lambda params: torch.mean(net(params) ** 2),
        lambda params: torch.mean(net(params) ** 2),
        lambda params: torch.mean(net(params) ** 2),
    ]

    num_epochs = 150
    print("Started Training!")
    for epoch in range(num_epochs):
        params = list(net.parameters())
        params = net.pinn_lr_annealing(L_r = net.L_r, L_i = L_i, xyt= xyt, params = params, S=1000)  # Add the 'S' parameter here

        # Update the weights
        for param, new_param in zip(net.parameters(), params):
            param.data.copy_(new_param)

        if epoch % 10 == 0:
            loss = net.L_r(xyt) + sum(lambda_i(xyt) for lambda_i in L_i)
            print(f"Epoch {epoch}, Loss: {loss.item()}")

    net.visualize()

















#####3



        #  optimizer = optim.Adam(net.parameters(), lr=0.001)
        # loss_fn = nn.MSELoss()
        # loss_arr = []
        # epoch_arr = []
        # #Training loop
        # num_epochs = 150
        # print("Started Training!")
        # for epoch in range(num_epochs):
        #     optimizer.zero_grad()

        #     # Predict the solution
        #     u_pred = net(xyt)
            

        #     # Calculate loss
        #     loss = loss_fn(u_pred, torch.zeros_like(u_pred))

        #     # Calculate gradients
        #     grads = torch.autograd.grad(outputs=loss, inputs=xyt, create_graph=True, grad_outputs=torch.ones_like(loss))[0]
        #     u_t = grads[:, 2]
        #     u_x = grads[:, 0]
        #     u_y = grads[:, 1]

        #     grads2 = torch.autograd.grad(outputs=u_x.sum(), inputs=xyt, create_graph=True, grad_outputs=torch.ones_like(u_x.sum()))[0]
        #     u_xx = grads2[:, 0]
        #     grads2 = torch.autograd.grad(outputs=u_y.sum(), inputs=xyt, create_graph=True, grad_outputs=torch.ones_like(u_y.sum()))[0]
        #     u_yy = grads2[:, 1]

        #     alpha = 0.01
        #     f = u_t - alpha * (u_xx + u_yy)

        #     loss = 5*torch.mean(f**2)

        #     # Apply boundary condition
        #     left_bc_loss = torch.mean((net(left_boundary) - 1.0)**2)
        #     right_bc_loss = torch.mean((net(right_boundary)**2))
        #     top_bc_loss = torch.mean((net(top_boundary)**2))
        #     bottom_bc_loss = torch.mean((net(bottom_boundary)**2))
            
        #     loss += 10*(left_bc_loss + right_bc_loss + top_bc_loss + bottom_bc_loss)

        #     # Apply initial conditions
        #     left_init_loss = torch.mean((net(left_init) - 1.0)**2)
        #     right_init_loss = torch.mean((net(right_init)**2))
        #     top_init_loss = torch.mean((net(top_init)**2))
        #     bottom_init_loss = torch.mean((net(bottom_init)**2))
        #     middle_init_loss = torch.mean((net(middle_init)**2))

        #     loss += left_init_loss + right_init_loss + top_init_loss + bottom_init_loss + middle_init_loss



        #     loss.backward()
        #     optimizer.step()

        #     loss_arr.append(loss.item())
        #     epoch_arr.append(epoch)

        #     #if epoch % 100 == 0:
        #     print(f"Epoch {epoch}, Loss: {loss.item()}")
            

        # net.visualize()
        # plot_loss(loss_arr,epoch_arr)