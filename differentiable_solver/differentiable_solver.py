import torch
import torch.autograd as autograd

import matplotlib.pyplot as plt
import matplotlib.animation as animation


x_min, x_max = -0.5 , 0.5
y_min, y_max = -0.5 , 0.5
t_max = 10
#visualization function
def visualize_solution(u, Nx, Ny, Nt):
    t = torch.linspace(0, t_max, Nt)
    fig, ax = plt.subplots()
    cax = ax.imshow(u[0].detach().numpy().T, cmap='jet', origin='lower',
                    extent=[x_min, x_max, y_min, y_max], vmin=0, vmax=1)
    cb = fig.colorbar(cax)

    def update(frame):
        ax.set_title(f"Time: {t[frame]:.2f}s")
        cax.set_array(u[frame].detach().numpy().T.reshape(int(Nx), int(Ny)))
        return cax,

    ani = animation.FuncAnimation(fig, update, frames=range(1, Nt), interval=100, blit=True)
    plt.show()


def exp_FDS(alpha,dx,dy,dt):
    #parametrs
    num_timesteps = int(t_max / dt)
    Nx = int((x_max - x_min) / dx) + 1
    Ny = int((y_max - y_min) / dy) + 1
    #initial u
    u = torch.zeros((num_timesteps, Nx, Ny), requires_grad=False)
    # Set initial condition
    u[0, :, :] = 0.0
    # Set boundary conditions
    u[:, 0, :] = 1.0
    u[:, -1, :] = 0.0
    u[:, :, 0] = 0.0
    u[:, :, -1] = 0.0
    # Shift the domain to [−0.5, 0.5]m × [−0.5, 0.5]m
    x_values = torch.linspace(-0.5, 0.5, Nx)
    # Determine the left boundary indices
    left_boundary_indices = (x_values == -0.5).nonzero(as_tuple=True)[0]
    # Explicit finite difference scheme
    for t in range(num_timesteps - 1):
        print(f"Time step {t}/{num_timesteps}")
        for i in range(1, Nx - 1):
            for j in range(1, Ny - 1):
                if i in left_boundary_indices:
                    u[t + 1, i, j] = 1.0
                else:
                    u[t + 1, i, j] = (
                        u[t, i, j]
                        + alpha * dt * (
                            (u[t, i + 1, j] - 2 * u[t, i, j] + u[t, i - 1, j]) / (dx * dx)
                            + (u[t, i, j + 1] - 2 * u[t, i, j] + u[t, i, j - 1]) / (dy * dy)
                        )
                    )
    return u

def inverse_problem(alpha, dx, dy, dt, observed_data, num_epochs, learning_rate):
    optimizer = torch.optim.Adam([alpha], lr=learning_rate)
    
    for epoch in range(num_epochs):
        u = exp_FDS(alpha, dx, dy, dt)
        probe_data = u[[0, int(1/dt), int(2/dt), int(4/dt)], (u.shape[1]-1)//2, (u.shape[2]-1)//2]
        loss = torch.mean((probe_data - observed_data) ** 2)
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        
        print(f"Epoch {epoch}/{num_epochs}, Loss: {loss.item()}, Alpha: {alpha.item()}")
    
    return alpha.item()

if __name__ == '__main__':


    alpha = autograd.Variable(torch.tensor(0.01), requires_grad=True)
    dt = 0.01
    dx = 0.05
    dy = 0.05
 
    Nt = int(t_max / dt)
    Nx = int((x_max - x_min) / dx) + 1
    Ny = int((y_max - y_min) / dy) + 1

    u = exp_FDS(alpha,dx,dx,dt)
    visualize_solution(u,Nx,Ny,Nt)


   

    # observed_data = torch.tensor([0.0, 0.1, 0.15, 0.25])
    # num_epochs = 5
    # learning_rate = 0.01

    # optimal_alpha = inverse_problem(alpha, dx, dy, dt, observed_data, num_epochs, learning_rate)
    # print("Optimal alpha:", optimal_alpha)
    

    #visualize_solution(u,Nx,Ny,num_timesteps)
