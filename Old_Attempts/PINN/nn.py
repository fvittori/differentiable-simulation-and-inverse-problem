import torch
from torch import nn
import numpy as np
from PINN.consts import x_min, x_max, y_min, y_max, t_min, t_max, num_epoch, N_boundary, N_interior
from random import uniform

import matplotlib.pyplot as plt
import matplotlib.animation as animation


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')



#DONE
class HeatNN(nn.Module):

    def __init__(self, X_u, u, X_f):
        super(HeatNN,self).__init__() 

        self.model = nn.Sequential(
                nn.Linear(3,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,1))
        

        #x from boundary conditions:
        self.x_u = torch.tensor(X_u[:, 0].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)
        #y from boundary conditions:
        self.y_u = torch.tensor(X_u[:, 1].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)
        #t from boundary conditions:
        self.t_u = torch.tensor(X_u[:, 2].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)

        # x from collocation points:
        self.x_f = torch.tensor(X_f[:, 0].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)
        #y from collocation points:
        self.y_f = torch.tensor(X_f[:, 1].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)

        # t from collocation points:
        self.t_f = torch.tensor(X_f[:, 2].reshape(-1, 1),
                                dtype=torch.float32,
                                requires_grad=True)




        
        #boundary solution
        self.u = torch.tensor(u,dtype=torch.float32)
        
        #null vector to test against f:
        self.null = torch.zeros((self.x_f.shape[0],1))
        
        # this optimizer updates the weights and biases of the net:
        self.optimizer = torch.optim.Adam(self.model.parameters(),
                                    lr=0.01)
        
        #why not
        self.loss = nn.MSELoss()

        #loss : 
        self.ls = 0

        #iteration number
        self.iter =0 

        self.alpha = 0.1



    def forward(self,x):
        return self.model(x)
    
    def pred_u(self,x,y,t):
        x.requires_grad = True
        y.requires_grad = True
        t.requires_grad = True
        return self.forward(torch.hstack((x,y,t)))
    
    def pred_f(self,x,y,t):
        
        x.requires_grad = True
        y.requires_grad = True
        t.requires_grad = True
        #make prediction
        u = self.forward(torch.hstack((x,y,t)))
        #get du/dt 
        #create graph to enable computation of second order deriv
        u_t = torch.autograd.grad(u, t,
                                grad_outputs=torch.ones_like(u),
                                retain_graph = True,
                                create_graph= True)[0]
        u_x = torch.autograd.grad(u, x,
                                grad_outputs=torch.ones_like(u),
                                retain_graph = True,
                                create_graph= True)[0]

        u_y = torch.autograd.grad(u, y,
                                grad_outputs=torch.ones_like(u),
                                retain_graph = True,
                                create_graph= True)[0]
        
        u_xx = torch.autograd.grad(u_x, x,
                                grad_outputs=torch.ones_like(u),
                                retain_graph = True,
                                create_graph= True)[0]
        u_yy = torch.autograd.grad(u_y, y,
                                grad_outputs=torch.ones_like(u),
                                retain_graph = True,
                                create_graph= True)[0]
        #calculate error 
        f = u_t - self.alpha * (u_xx + u_yy)
        return f
    

    def closure(self):
        # reset gradients to zero:
                
        self.optimizer.zero_grad()

        self.model.train(True)
        # u & f predictions:
        u_prediction = self.pred_u(self.x_u, self.y_u, self.t_u)
        f_prediction = self.pred_f(self.x_f, self.y_f, self.t_f)

        # losses:
        u_loss = self.loss(u_prediction, self.u)
        f_loss = self.loss(f_prediction, self.null)
        self.ls = u_loss + f_loss

        # derivative with respect to net's weights:
        self.ls.backward()

        # increase iteration count:
        self.iter += 1

        # print report:
        if not self.iter % 100:
            print('Epoch: {0:}, Loss: {1:6.3f}'.format(self.iter, self.ls))
            print(f'Prediction on u: {u_prediction}')
            print(f'Prediction on f: {f_prediction}')



        return self.ls


    def train(self, max_iter=num_epoch):
        self.model.train()

        def closure():
            # reset gradients to zero:
            self.optimizer.zero_grad()

            # u & f predictions:
            u_prediction = self.pred_u(self.x_u, self.y_u, self.t_u)
            f_prediction = self.pred_f(self.x_f, self.y_f, self.t_f)

            # losses:
            u_loss = self.loss(u_prediction, self.u)
            f_loss = self.loss(f_prediction, self.null)
            self.ls = u_loss + f_loss

            # derivative with respect to net's weights:
            self.ls.backward()

            # increase iteration count:
            self.iter += 1

            # print report:
            if not self.iter % 100:
                print('Epoch: {0:}, Loss: {1:6.3f}'.format(self.iter, self.ls))

            return self.ls

        for _ in range(max_iter):
            self.optimizer.step(closure)

    
    def visualize_solution_nn(self):

        x = np.arange(x_min, x_max, 1/10)
        y = np.arange(y_min, y_max, 1/10)
        t = np.arange(0, t_max, 1)

        x_tensor = torch.tensor(x, dtype=torch.float32)
        y_tensor = torch.tensor(y, dtype=torch.float32)
        t_tensor = torch.tensor(t, dtype=torch.float32)


        X_tensor, Y_tensor, T_tensor = torch.meshgrid(x_tensor, y_tensor, t_tensor)

        input_tensor = torch.stack([X_tensor, Y_tensor, T_tensor], dim=-1).reshape(-1, 3)

        heat = self.model(input_tensor).view(X_tensor.shape).detach().cpu().numpy()

        fig, ax = plt.subplots()
        cax = ax.imshow(heat[:, :, 0], cmap='jet', origin='lower',
                        extent=[x_min,x_max,y_min,y_max], vmin=0, vmax=1)
        cb = fig.colorbar(cax)

        def update(frame):
            ax.set_title(f"Time: {t[frame]:.2f}s")
            cax.set_array(heat[:, :, frame].reshape(len(x),len(y)))
            return cax,

        ani = animation.FuncAnimation(fig, update, frames=range(1, len(t)), interval=100, blit=True)
        plt.show()


def generate_data(N_u, N_f):
    
    N_u = N_u//2

    #Boundary Conditions
    x_bc_left = np.zeros((N_u//4, 1), dtype=float) - x_max
    y_bc_left = np.random.rand(N_u//4,1) - y_max
    t_bc_left = np.random.rand(N_u//4,1) * t_max
   
    x_bc_right = np.zeros((N_u//4, 1), dtype=float) + x_max
    y_bc_right = np.random.rand(N_u//4,1) - y_max
    t_bc_right = np.random.rand(N_u//4,1) * t_max
   
    x_bc_top = np.random.rand(N_u//4,1) - x_max
    y_bc_top = np.zeros((N_u//4, 1), dtype=float) + y_max
    t_bc_top = np.random.rand(N_u//4,1) * t_max
   
    x_bc_bot = np.random.rand(N_u//4,1) - x_max
    y_bc_bot = np.zeros((N_u//4, 1), dtype=float) - y_max
    t_bc_bot = np.random.rand(N_u//4,1) * t_max
   

    u_bc_right = np.zeros_like(x_bc_right)
    u_bc_top   = np.zeros_like(x_bc_top)
    u_bc_bot   = np.zeros_like(x_bc_bot)
    u_bc_left  = np.ones_like(x_bc_left)

    x_bc = np.vstack((x_bc_left,x_bc_right,x_bc_top, x_bc_bot))
    y_bc = np.vstack((y_bc_left,y_bc_right,y_bc_top, y_bc_bot))
    t_bc = np.vstack((t_bc_left,t_bc_right,t_bc_top, t_bc_bot))
    u_bc = np.vstack((u_bc_left,u_bc_right,u_bc_top, u_bc_bot))


  

    #Initial Conditions
    x_zero_left = np.zeros((N_u//4, 1), dtype=float) - x_max
    y_zero_left = np.random.rand(N_u//4,1) - y_max
    t_zero_left = np.zeros((N_u//4, 1), dtype=float) 
   
    
    x_zero_right = np.zeros((N_u//4, 1), dtype=float) + x_max
    y_zero_right = np.random.rand(N_u//4,1) - y_max
    t_zero_right = np.zeros((N_u//4, 1), dtype=float) 
    
    x_zero_top = np.random.rand(N_u//4,1) - x_max
    y_zero_top = np.zeros((N_u//4, 1), dtype=float) + y_max
    t_zero_top = np.zeros((N_u//4, 1), dtype=float) 
    
    x_zero_bot = np.random.rand(N_u//4,1) - x_max
    y_zero_bot = np.zeros((N_u//4, 1), dtype=float) - y_max
    t_zero_bot = np.zeros((N_u//4, 1), dtype=float) 

    u_zero_right = np.zeros_like(x_zero_right)
    u_zero_top   = np.zeros_like(x_zero_top)
    u_zero_bot   = np.zeros_like(x_zero_bot)
    u_zero_left  = np.ones_like(x_zero_left)
    

    x_zero = np.vstack((x_zero_left,x_zero_right,x_zero_top, x_zero_bot))
    y_zero = np.vstack((y_zero_left,y_zero_right,y_zero_top, y_zero_bot))
    t_zero = np.vstack((t_zero_left,t_zero_right,t_zero_top, t_zero_bot))
    u_zero = np.vstack((u_zero_left,u_zero_right,u_zero_top, u_zero_bot))


    A_bc = np.hstack( (x_bc, y_bc, t_bc) )
    A_zero = np.hstack( (x_zero, y_zero, t_zero) )



 
 
    # each one of these three arrays haS 3 columns, 
    # now we stack them vertically, the resulting array will also have 3
    # columns and 100 rows:
    A_u_train = np.vstack((A_bc, A_zero))
    
    # shuffle A_u_train:
    index = np.arange(0, N_u)
    np.random.shuffle(index)
    A_u_train = A_u_train[index, :]

    # make A_f_train
    A_f_train = np.zeros((N_f, 3), dtype=float)
    for row in range(N_f):
        x = uniform(x_min, x_max)  # x range
        y = uniform(y_min, y_max)  # x range
        t = uniform(t_min, t_max)  # t range

        A_f_train[row, 0] = x 
        A_f_train[row, 1] = y 
        A_f_train[row, 2] = t
    
    
    # add the boundary points to the collocation points:
    A_f_train = np.vstack((A_f_train, A_u_train))
    


    # stack them in the same order as X_u_train was stacked:
    u_train = np.vstack((u_bc,u_zero))
    # match indices with X_u_train
    u_train = u_train[index, :]

    return A_u_train, u_train, A_f_train


def create_heat_nn(A_u_train, u_train, A_f_train):
    return HeatNN(A_u_train, u_train, A_f_train)


def train_heat_nn(pinn):
    pinn.train()

if __name__ == '__main__' :
     

    A_u_train, u_train, A_f_train = generate_data(N_boundary,N_interior)
    pinn = create_heat_nn(A_u_train, u_train, A_f_train)
    train_heat_nn(pinn)
    pinn.visualize_solution_nn()






























# #DONE
# def train_step(alpha, model, optimizer, interior, boundary, initial):
    
#     optimizer.zero_grad()

   
#     x, y, t = interior
#     x = torch.tensor(x, dtype=torch.float32, device=device)
#     y = torch.tensor(y, dtype=torch.float32, device=device)
#     t = torch.tensor(t, dtype=torch.float32, device=device)
#     interior_loss = pde_loss(x, y, t, alpha, model)

#     # Boundary
#     x, y, t = boundary
#     x = torch.tensor(x, dtype=torch.float32, device=device)
#     y = torch.tensor(y, dtype=torch.float32, device=device)
#     t = torch.tensor(t, dtype=torch.float32, device=device)
#     u_actual = torch.tensor(utils.g(x.cpu().numpy(), y.cpu().numpy(), t.cpu().numpy()), dtype=torch.float32, device=device)

#     u_pred = model(torch.stack([x,y,t]).t())


#     # print("x Vector")
#     # print(x)
#     # print("Predicted Value")
#     # print(u_pred)
#     # print("Actual Value")
#     # print(u_actual)

#     boundary_loss = torch.mean((u_pred - u_actual) ** 2)

#     # Initial
#     x, y, t = initial
#     x = torch.tensor(x, dtype=torch.float32, device=device)
#     y = torch.tensor(y, dtype=torch.float32, device=device)
#     t = torch.zeros_like(x, device=device)
#     u_actual = torch.tensor(utils.u_0(x.cpu().numpy(), y.cpu().numpy()), dtype=torch.float32, device=device)
#     u_pred = model(torch.stack([x,y,t]).t())
#     initial_condition_loss = torch.mean((u_pred - u_actual) ** 2)

#     # Calculate overall loss
#     loss =  boundary_loss + initial_condition_loss + interior_loss

#     # Get gradient from loss
#     loss.backward()
#     optimizer.step()

#     return loss.item()
