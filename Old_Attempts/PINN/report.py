import torch
from torch import nn

import PINN.consts as consts




device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')



#DONE
class HeatNN(nn.Module):

    def __init__(self):
        super(HeatNN,self).__init__() 

        self.layers = nn.Sequential(
                nn.Dropout(0.2),
                nn.Linear(3,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,100),
                nn.Tanh(),
                nn.Linear(100,1))

    



    def forward(self,x):
        return self.layers(x)
    
    

#DONE
def pde_loss(x,y,t,alpha,model):

    #Enable gradient tracking
    x,y,t = x.requires_grad_(True), y.requires_grad_(True), t.requires_grad_(True)



    #make prediction
    u_pred = model(torch.stack([x,y,t]).t())
    #get du/dt 
    #create graph to enable computation of second order deriv
    u_pred_grad = torch.autograd.grad(u_pred,
                                      (x,y,t), 
                                      create_graph=True)

    #get values
    u_x = u_pred_grad[0]
    u_y = u_pred_grad[1]
    u_t = u_pred_grad[2]


    #calculate second order derivative
    u_xx = torch.autograd.grad(u_x,
                               x,
                               create_graph=True)[0]
    u_yy = torch.autograd.grad(u_y,
                               y,
                               create_graph=True)[0]

    #calculate error 
    pde_term = u_t - alpha * (u_xx + u_yy)
    return torch.mean(pde_term**2)




#DONE
def train_step(alpha, model, optimizer, interior, boundary, initial):
    
    optimizer.zero_grad()

   
    x, y, t = interior
    x = torch.tensor(x, dtype=torch.float32, device=device)
    y = torch.tensor(y, dtype=torch.float32, device=device)
    t = torch.tensor(t, dtype=torch.float32, device=device)
    interior_loss = pde_loss(x, y, t, alpha, model)

    # Boundary
    x, y, t = boundary
    x = torch.tensor(x, dtype=torch.float32, device=device)
    y = torch.tensor(y, dtype=torch.float32, device=device)
    t = torch.tensor(t, dtype=torch.float32, device=device)
    u_actual = torch.tensor(utils.g(x.cpu().numpy(), y.cpu().numpy(), t.cpu().numpy()), dtype=torch.float32, device=device)

    u_pred = model(torch.stack([x,y,t]).t())


    # print("x Vector")
    # print(x)
    # print("Predicted Value")
    # print(u_pred)
    # print("Actual Value")
    # print(u_actual)

    boundary_loss = torch.mean((u_pred - u_actual) ** 2)

    # Initial
    x, y, t = initial
    x = torch.tensor(x, dtype=torch.float32, device=device)
    y = torch.tensor(y, dtype=torch.float32, device=device)
    t = torch.zeros_like(x, device=device)
    u_actual = torch.tensor(utils.u_0(x.cpu().numpy(), y.cpu().numpy()), dtype=torch.float32, device=device)
    u_pred = model(torch.stack([x,y,t]).t())
    initial_condition_loss = torch.mean((u_pred - u_actual) ** 2)

    # Calculate overall loss
    loss =  boundary_loss + initial_condition_loss + interior_loss

    # Get gradient from loss
    loss.backward()
    optimizer.step()

    return loss.item()
