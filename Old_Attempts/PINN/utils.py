import numpy as np
import torch
import matplotlib.pyplot as plt
import Classical_FDM.consts as consts
from Classical_FDM.consts import Nx, Ny



#source term
def f(x,y,t):
    return np.zeros_like(x)


#boundary term
def g(x,y,t):
     result = np.zeros_like(x)
     result[x== -0.5] = 1
     return result


#Initial state of the system u(x,y,0) = 0
#except (x,y) = (-0.5,y) = 1
def u_0(x,y):
     result = np.zeros_like(x)
     result[x== -0.5] = 1
     return result



#sample points from (x,y) \in [-0.5 , 0.5] x [-0.5 , 0.5]
def sample_points(N):
    x = np.random.rand(N,1) - .5
    y = np.random.rand(N,1) - .5 
    t = np.random.rand(N,1) * consts.T 
    return x,y,t

#get boundary points
def boundary_points(N): 
    x_boundary = np.random.rand(N,1)
    y_boundary = np.random.rand(N,1)
    t_boundary = np.random.rand(N,1)* consts.T 

    choice = np.random.choice(4,N)
    
    x_boundary[choice == 0] = -0.5 
    x_boundary[choice == 1] = 0.5
    y_boundary[choice == 2] = -0.5 
    y_boundary[choice == 3] = 0.5

    return x_boundary, y_boundary, t_boundary
 

def visualize_solution_nn(model, nx, ny, nt):
    fig, ax = plt.subplots()
    cax = ax.imshow(np.zeros((nx, ny)), cmap='hot', vmin=0, vmax=1, extent=[-0.5, 0.5, -0.5, 0.5])

    def update(frame):
        model_input = torch.tensor([[frame, x, y] for x in range(nx) for y in range(ny)]).float()
        with torch.no_grad():
            predictions = model(model_input).reshape(nx, ny).detach().numpy()
        cax.set_array(predictions.ravel())
        return [cax]

    ani = animation.FuncAnimation(fig, update, frames=range(0, nt), interval=100, blit=True)

    plt.colorbar(cax)
    plt.show()



