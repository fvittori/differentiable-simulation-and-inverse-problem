import numpy as np

def exp_FD(u, alpha, nx, ny, nt):
    dx = (x_max - x_min) / (nx - 1)
    dy = (y_max - y_min) / (ny - 1)
    dt = T / (nt - 1)
    
    u_new = np.zeros_like(u)

    for k in range(0, nt-1):
        for i in range(1, nx-1):
            for j in range(1, ny-1):
                u_new[k+1, i, j] = u[k, i, j] + alpha*dt*((u[k, i+1, j] - 2*u[k, i, j] + u[k, i-1, j])/dx**2 +
                                                       (u[k, i, j+1] - 2*u[k, i, j] + u[k, i, j-1])/dy**2)
    return u_new

def init_domain(x_min, x_max, y_min, y_max, T, nx, ny, nt):
    dx = (x_max - x_min) / (nx - 1)
    dy = (y_max - y_min) / (ny - 1)
    dt = T / (nt - 1)

    u_tmp = np.zeros((nt, nx, ny))
    u_tmp[0, :, :] = 0
    u_tmp[:, :, 0] = 100

    return u_tmp, dx, dy, dt


def error_function(u_observed, u_model):
    return np.sum((u_observed - u_model)**2)

def gradient_error_function(u_observed, u_model, u_alpha_gradient):
    return np.sum(2 * (u_observed - u_model) * u_alpha_gradient)


def exp_FD_gradient(u,dx, dy, dt):
    u_alpha_gradient = np.zeros_like(u)

    nt, nx, ny = u.shape
    for k in range(0, nt-1):
        for i in range(1, nx-1):
            for j in range(1, ny-1):
                u_alpha_gradient[k+1, i, j] = dt * ((u[k, i+1, j] - 2*u[k, i, j] + u[k, i-1, j])/dx**2 +
                                                    (u[k, i, j+1] - 2*u[k, i, j] + u[k, i, j-1])/dy**2)
    return u_alpha_gradient



def optimize_alpha(u_observed,t_values, x_min, x_max, y_min, y_max, T, nx, ny, nt, alpha_init, learning_rate, num_iterations):
    u, dx, dy, dt = init_domain(x_min, x_max, y_min, y_max, T, nx, ny, nt)
    alpha = alpha_init
    
    # Middle point indices
    i, j = nx // 2, ny // 2
    
    t_indices = np.where(np.isin(np.arange(nt), t_values))[0]

    for _ in range(num_iterations):
        u_model = exp_FD(u, alpha, nx, ny, nt)
        
        u_alpha_gradient = exp_FD_gradient(u, dx, dy, dt)
        
        u_model_middle = u_model[t_indices, i, j]
        u_alpha_gradient_middle = u_alpha_gradient[t_indices, i, j]
        

        gradient = gradient_error_function(u_observed, u_model_middle, u_alpha_gradient_middle)
        print(gradient)
        alpha -= learning_rate * gradient
        print(alpha)
    
    return alpha

# Parameters
t_values = np.array([0, 1, 2, 4])
u_observed = np.array([0, 0.1, 0.15, 0.25])

x_min, x_max = -0.5, 0.5 # Range for the x-dimension
y_min, y_max = -0.5, 0.5 # Range for the y-dimension
T = 10 # Maximum time


nx, ny, nt = 50, 50, 50 # Number of grid points in each dimension
alpha_init = 1.0
learning_rate = 0.001
num_iterations = 1000


# Optimize alpha
alpha_optimal = optimize_alpha(u_observed, t_values, x_min, x_max, y_min, y_max, T, nx, ny, nt, alpha_init, learning_rate, num_iterations)
print(f"Optimal alpha: {alpha_optimal}")