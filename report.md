# EIxercise 1


## Methods
The heat equation is given by:
$$\frac{\partial u}{\partial t} = \alpha \frac{\partial^2 u}{\partial x^2}$$
To derive the implicit finite difference scheme, let's discretize the spatial domain using $N$ points $(x_0, x_1, ..., x_n)$ and the time domain using $M$ points $(t_0, t_1, ..., t_m)$. Let $u_{i,j}$ be the numerical approximation to the exact solution $$u(x_i, t_j$$.

Forward difference approximation can be used for the time derivative, and a central difference approximation for the spatial second derivative:
$$\frac{u_{i,j} - u_{i,j-1}}{\Delta t} = \alpha \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{\Delta x^2}$$
The implicit finite difference scheme is called "implicit" because the equation above involves the unknowns $u_{i,j}$, $u_{i+1,j}$, and $u_{i-1,j}$ at the same time level. This leads to a system of linear equations that must be solved simultaneously to find the solution at each time step.

## Explicit Finite Difference Scheme
The explicit finite difference scheme can also be derived from the heat equation:
$$\frac{\partial u}{\partial t} = \alpha \frac{\partial^2 u}{\partial x^2}$$
For the explicit scheme, we use forward difference approximation for the time derivative and central difference approximation for the spatial second derivative. However, we evaluate the spatial derivative at the previous time level:
$$\frac{u_{i,j+1} - u_{i,j}}{\Delta t} = \alpha \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{\Delta x^2}$$
The explicit finite difference scheme is called "explicit" because the equation above can be directly used to compute the value of $u_{i,j+1}$ using the known values at the previous time level.

## Crank-Nicolson Scheme

The Crank-Nicolson scheme is a combination of the implicit and explicit finite difference schemes. It uses an average of the explicit and implicit schemes to approximate the heat equation:
$\frac{\partial u}{\partial t} = \alpha \frac{\partial^2 u}{\partial x^2}$
To derive the Crank-Nicolson scheme, we use the average of the spatial second derivatives at time levels $j$ and $j+1$:
$$\frac{u_{i,j+1} - u_{i,j}}{\Delta t} = \frac{\alpha}{2} \left(\frac{u_{i+1,j+1} - 2u_{i,j+1} + u_{i-1,j+1}}{\Delta x^2} + \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{\Delta x^2}\right)$$
The Crank-Nicolson scheme is second-order accurate in both time and space and is unconditionally stable. However, like the implicit scheme, it requires solving a system of linear equations at each time step.



## Discussion
At the beginning their were some issues with changing the values directly if 'requires\_grad==True' this was then fixed by creating temporary variables.
Another issue arised from too much memory usage for to small $dx,dy,dt$ this was fixed by increasing the size. Better hardware would of course allow for more precise solutions. 

Then which is an issue in the last exercise, I struggled with computing the gradient using torch library. Different approaches were used, as one can see in the files, but none seemed to work.

Finally I managed to solve rewrite the explicit finite difference scheme to work with autodiff. The solution can be found in "differentiable_solver.py"

Experiments we're left out as I ran out of time.



# Exercise 2

## Methods

PINNs refer to a type of neural network that fits data points provided by boundary and initial conditions, while also penalizing solutions whose partial derivatives do not satisfy the desired PDE. This characteristic makes PINNs a mesh-free and stable alternative to traditional numerical methods for solving PDEs.

The loss function employed in Physics-Informed Neural Networks (PINNs) is composed of two terms: the output values loss and the PDE regularization term. The output values loss term penalizes the model when it fails to predict the correct output values at specific points, which are usually chosen based on the initial and boundary conditions of the problem. Any loss function, such as mean squared error, can be utilized to measure the discrepancy between the predicted values and the ground truth values.

## Discussion

A lot of issues arised during the implementation of this method. 
In my first run the Neural Network didn't seem to learn the BC and overall converged to a constant.
This could be due to one main reason: The BC weren't setup correctly, thus the NN only trained on the PDE, which certainly allows for constants as a result, as the derivatives then equal to $0$.

In my second attempt I got inspired by following repo: https://github.com/EdgarAMO/PINN-Burgers
I changed the code to work 2D and the Heat Equation.
For this attempt the results weren't satisfying as the loss seemed to converge very slowly.
Reasons for this could be the complicated structure of the NN combined with limited hardware resources. Further testing on better machines would be needed.

In my third and last attempt I simplified things and used a lot of the given torch functions, which to no surprise, were quite helpful. Simplifying the model lead to satsifying convergence of the loss.

3 hidden layers with 100 neurons each were used.

Setting the parameters "high" was not successful.
For a Gird with (25,25,25) I seemed to run out of memory at around 250 epochs.
Again better hardware would allow for more accurate computations.

Thus I limited myself to a (25,25,10) grid with 
250 epochs.

3 hidden layers with 100 neurons each were used.

The results, were mildly satisfying from a visual perspective as the NN didn't seem to capture the inital conditions and boundary values on the left side correctly.
It again also seemd that the NN was approximating a constant.

This produces following graph:

![Loss_a1](Loss_a1.png)



I then tried to enforce the boundary condition on the left side by weighting it more than the other BC.
It seemed that the solution converged to a periodic function as the middle points $x\in[0,0.5]$ repeated the same values as $x\in[-0.5,0]$.
![Loss_a1](Loss_a2.png)

To this end I realized that I didn't specifiy BC in the middle, thus allowing for such behaviour. This was then fixed.
The behaviour didn't seem to change. 
I then realized that $\tanh$ might not be the best activation for such a task due to it being "too smooth".
I then switched to a ReLU, which _seemed_ removed the periodic behaviour but in turn caused it to produced very fragmented results with no regards to the BC. The loss seemed to converge fast with ReLU.

![Loss_a3](Loss_a3.png)

Now weighting the BC again produced the same periodics behaviour.

![Loss_a4](Loss_4.png)

Doubling the hidden layers $3\to6$ caused better visual results. Which is typical behaviour in a NN:
![Loss_a5](Loss_a5.png)

Again double the hidden layers from $6\to12$ caused better visual results, but not satisfying.

![Loss_a6](Loss_a6.png)

This was done with grid = (30,30,30)

Overall further research has to be done on how to weight BC properly such that the NN adapts it. 
Other improvements can be done on the sampling, like sampling more BC data - points or or trying a more complex sampling scheme.

# Exercise 3

## Methods

In my first attempt I wrote a program hich produced a constant alpha. My guess is that the my FDM functions don't manage to calculate the gradient wrt. $\alpha$ thus yielding a gradient equal to None or Zero.
To this end I tried to use a wrapper. Which didn't help. 
Multiple approaches were used but none of them seemed to be able to calculate the gradient correctly.
I even tried implementing the algorithm in pure numpy, sadly this didn't work either.
To this end I had following reasoning:
In order to find the optimal value for $\alpha$, we will need to minimize the error between the observed data points and the model's output. In this case, we can use gradient descent to minimize the error by updating the $\alpha$ value using its gradient with respect to the error.

We will first define the error function $E(\alpha)$, which represents the sum of squared differences between the observed data points and the model's output:

\begin{equation}
E(\alpha) = \sum_{i=1}^{n}(u_{observed}^{(i)} - u_{model}^{(i)})^2
\end{equation}

Then, we will compute the gradient of the error function with respect to $\alpha$ and update $\alpha$ using gradient descent:

\begin{equation}
\alpha = \alpha - \eta \frac{\partial E(\alpha)}{\partial \alpha}
\end{equation}

where $\eta$ is the learning rate.

To find the gradient $\frac{\partial E(\alpha)}{\partial \alpha}$, we can use the chain rule:

\begin{equation}
\frac{\partial E(\alpha)}{\partial \alpha} = \sum_{i=1}^{n} 2(u_{observed}^{(i)} - u_{model}^{(i)})\frac{\partial u_{model}^{(i)}}{\partial \alpha}
\end{equation}


All the previous code can be found in the folder 'Old'.

One main issue which was also found in previous implementation was that the Tensor _u_ had set _requires\_grad=True_ which lead to the issue of not being able to copy assign any values to _u_, as this would not allow for gradient calculations. It was later realized that this option should have benn set to _False_ as we're not calculating gradients w.r.t. _u_ but $\alpha$.

The final code seemed to produces satisfying results, this is due to the fact that the 'exp_FD' function now properly is able to calculate the gradient.
The final code uses Adam as an optimizer.
Different optimizers weren't tested.
Declaring 
    alpha = autograd.Variable(torch.tensor(0.01), requires_grad=True)

Helped with the debugging and produced satisfying convergence.

The final result was:
$$\alpha = 0.5$$

# Final Remarks

This exercise once again showed me that programming sometimes needs some distance. Sleeping over the exercises, which yesterday seemed impossible, allowed for me to solve them in a fraction of the time I spent on them yesterday. 

Sadly, as yesterday was spent with debuggin and tryining different approaches little to none numerical benchmarks have been performed.

If this project woudl continue I would try out different optimizer, NN-Architectures and different solvers, while comparing their results on convergence and speed.





