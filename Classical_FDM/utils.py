import torch


import matplotlib.pyplot as plt
import matplotlib.animation as animation


from consts import x_min, x_max, y_min, y_max


#Grid 3D grid where one dimension is for the time
#(x,y) \in [-0.5, 0.5] x [-0.5, 0,5]
#t \in consts.T (10)
def init_domain(nx,ny,nt):
    u_tmp = torch.zeros((nt,nx,ny))
    #intial conditon
    u_tmp[0:,:, :] = 0
    #boundary conditions
    u_tmp[:,:,0] = 100
    #Do this as we want to be able to calculate the gradient
    u = torch.zeros((nt,nx,ny),requires_grad=True)
    u.data = u_tmp.data
    return u


#visualization function
def visualize_solution(u,x,y,t):
    fig, ax = plt.subplots()
    cax = ax.imshow(u[0].detach().numpy(), cmap='jet', origin='lower',
                    extent=[x_min, x_max, y_min, y_max], vmin=0, vmax=1)
    cb = fig.colorbar(cax)

    def update(frame):
        ax.set_title(f"Time: {t[frame]:.2f}s")
        cax.set_array(u[frame].detach().numpy().reshape(len(x),len(y)))
        return cax,

    ani = animation.FuncAnimation(fig, update, frames=range(1, len(t)), interval=100, blit=True)
    plt.show()










