import torch


from consts import dx, dy, dt


#Explicit Finite Difference
def exp_FD(u,alpha,nx,ny,nt):

    u_new = torch.zeros_like(u)
    u_new.data = u.data


    # Explicit finite difference
    for k in range(0, nt-1):
        #print(f"Time step: {k+1}/{nt-1}")
        for i in range(1, nx-1):
            for j in range(1, ny-1):
                u_new[k+1, i, j] = u[k, i, j] + alpha*dt*((u[k, i+1, j] - 2*u[k, i, j] + u[k, i-1, j])/dx**2 +
                                                       (u[k, i, j+1] - 2*u[k, i, j] + u[k, i, j-1])/dy**2)
    
    
    
    return u_new

def exp_FD_alpha(u, alpha, nx, ny, nt):
    u_new = torch.zeros_like(u)
    u_new.data = u.data

  

    # Define the coefficient for the finite difference scheme
    coef = alpha * dt / dx**2

    # Explicit finite difference
    for k in range(0, nt - 1):
        for i in range(1, nx - 1):
            for j in range(1, ny - 1):
                u_new[k + 1, i, j] = u[k, i, j] + coef * ((u[k, i + 1, j] - 2 * u[k, i, j] + u[k, i - 1, j]) +
                                                          (u[k, i, j + 1] - 2 * u[k, i, j] + u[k, i, j - 1]))

    # Compute gradient of output tensor with respect to alpha
    grad = torch.autograd.grad(u_new.sum(), alpha, create_graph=True)[0]

    # Attach gradient to output tensor
    u_new.grad = grad

    return u_new

def crank_nicolson(u, alpha, nx, ny, nt):

    u_new = torch.zeros_like(u)
    u_new.data = u.data

    # Create matrices for the Crank-Nicolson method
    A = torch.eye(nx*ny) * (1 + 2*alpha*dt*(1/dx**2 + 1/dy**2))
    B = torch.eye(nx*ny) * (1 - 2*alpha*dt*(1/dx**2 + 1/dy**2))

    for i in range(1, nx-1):
        for j in range(1, ny-1):
            idx = j + i*ny
            A[idx, idx-ny] = -alpha*dt/dx**2
            A[idx, idx+ny] = -alpha*dt/dx**2
            A[idx, idx-1] = -alpha*dt/dy**2
            A[idx, idx+1] = -alpha*dt/dy**2

            B[idx, idx-ny] = alpha*dt/dx**2
            B[idx, idx+ny] = alpha*dt/dx**2
            B[idx, idx-1] = alpha*dt/dy**2
            B[idx, idx+1] = alpha*dt/dy**2

    # Crank-Nicolson method
    for k in range(0, nt-1):
        print(f"Time step: {k+1}/{nt-1}")

        u_flat = u[k].view(-1)
        u_new_flat = torch.linalg.solve(A, torch.matmul(B, u_flat))

        u_new_temp = u_new_flat.view(nx, ny)

        # Apply the boundary conditions at each time step
       
        u_new_temp[-1, :] = 0
        u_new_temp[:, -1] = 0
        u_new_temp[0, :] = 0
        u_new_temp[:, 0] = 1

        u_new[k+1] = u_new_temp


    if u.grad is not None:
        u_new.grad = u.grad

    return u_new



def imp_FD(u, alpha, nx, ny, nt):
    # Initialize u_tmp for boundary conditions

    

    u_bc = torch.zeros_like(u)
    u_bc[:, :, 0] = 100

    mask = torch.ones((nt, nx, ny), dtype=torch.bool)
    mask[:, :, 0] = 0
    mask[:, 0, :] = 0
    mask[:, -1, :] = 0
    mask[:, :, -1] = 0

    dt = 1.0 / (nt - 1)
    dx = 1.0 / (nx - 1)
    dy = 1.0 / (ny - 1)
    rx = alpha.item() * dt / (2 * dx**2)
    ry = alpha.item() * dt / (2 * dy**2)

    for t in range(0, nt - 1):
        u_new = u.clone()

        # Solve the x-direction implicit step
        A_x = torch.zeros((nx, nx))
        A_x += torch.diag(torch.full((nx-1,), -rx), diagonal=1)
        A_x += torch.diag(torch.full((nx,), 1 + 2 * rx))
        A_x += torch.diag(torch.full((nx-1,), -rx), diagonal=-1)
        LU_x, pivots_x = torch.linalg.lu_factor(A_x)
        
        for j in range(1, ny - 1):
            b_x = u[t, :, j].clone()
            u_x = torch.linalg.lu_solve(LU_x, pivots_x, b_x.unsqueeze(1))
            u_new[t + 1, :, j] = u_x.squeeze()

        # Solve the y-direction implicit step
        A_y = torch.zeros((ny, ny))
        A_y += torch.diag(torch.full((ny-1,), -ry), diagonal=1)
        A_y += torch.diag(torch.full((ny,), 1 + 2 * ry))
        A_y += torch.diag(torch.full((ny-1,), -ry), diagonal=-1)
        LU_y, pivots_y = torch.linalg.lu_factor(A_y)
        
        for i in range(1, nx - 1):
            b_y = u_new[t + 1, i, :].clone()
            u_y = torch.linalg.lu_solve(LU_y, pivots_y, b_y.unsqueeze(1))
            u_new[t + 1, i, :] = u_y.squeeze()

        # Add the boundary conditions to the computed solution
        u.data[mask] = u_new.data[mask]

    return u


# class ImpFDFunction(torch.autograd.Function):
#     @staticmethod
#     def forward(ctx, u, alpha, nx, ny, nt):
#         # Initialize u_tmp for boundary conditions
#         u_bc = torch.zeros_like(u)
#         u_bc[:, :, 0] = 100

#         mask = torch.ones((nt, nx, ny), dtype=torch.bool)
#         mask[:, :, 0] = 0
#         mask[:, 0, :] = 0
#         mask[:, -1, :] = 0
#         mask[:, :, -1] = 0

#         dt = 1.0 / (nt - 1)
#         dx = 1.0 / (nx - 1)
#         dy = 1.0 / (ny - 1)
#         rx = alpha.item() * dt / (2 * dx**2)
#         ry = alpha.item() * dt / (2 * dy**2)

#         for t in range(0, nt - 1):
#             u_new = u.clone()

#             # Solve the x-direction implicit step
#             A_x = torch.zeros((nx, nx))
#             A_x += torch.diag(torch.full((nx-1,), -rx), diagonal=1)
#             A_x += torch.diag(torch.full((nx,), 1 + 2 * rx))
#             A_x += torch.diag(torch.full((nx-1,), -rx), diagonal=-1)
#             LU_x, pivots_x = torch.linalg.lu_factor(A_x)

#             for j in range(1, ny - 1):
#                 b_x = u[t, :, j].clone()
#                 u_x = torch.linalg.lu_solve(LU_x, pivots_x, b_x.unsqueeze(1))
#                 u_new[t + 1, :, j] = u_x.squeeze()

#             # Solve the y-direction implicit step
#             A_y = torch.zeros((ny, ny))
#             A_y += torch.diag(torch.full((ny-1,), -ry), diagonal=1)
#             A_y += torch.diag(torch.full((ny,), 1 + 2 * ry))
#             A_y += torch.diag(torch.full((ny-1,), -ry), diagonal=-1)
#             LU_y, pivots_y = torch.linalg.lu_factor(A_y)

#             for i in range(1, nx - 1):
#                 b_y = u_new[t + 1, i, :].clone()
#                 u_y = torch.linalg.lu_solve(LU_y, pivots_y, b_y.unsqueeze(1))
#                 u_new[t + 1, i, :] = u_y.squeeze()

#             # Add the boundary conditions to the computed solution
#             u.data[mask] = u_new.data[mask]

#         ctx.save_for_backward(u, alpha)
#         ctx.nx = nx
#         ctx.ny = ny
#         ctx.nt = nt

#         return u

#     @staticmethod
#     def backward(ctx, grad_output):
#         u, alpha = ctx.saved_tensors
#         nx, ny, nt = ctx.nx, ctx.ny, ctx.nt

#         # Compute gradient of loss w.r.t. alpha
#         eps = 1e-6
#         alpha_p = alpha + eps
#         alpha_m = alpha - eps
#         u_p = ImpFDFunction.apply(u, alpha_p, nx, ny, nt)
#         u_m = ImpFDFunction.apply(u, alpha_m, nx, ny, nt)
#         loss_p = torch.sum((u_p - grad_output)**2)
#         loss_m = torch.sum((u_m - grad_output)**2)
#         d_alpha = (loss_p - loss_m) / (2 * eps)

#         # Compute gradients w.r.t. other inputs (not needed in this case)
#         d_u = None
#         d_nx = None
#         d_ny = None
#         d_nt = None

#         return d_u, d_alpha, d_nx, d_ny, d_nt