

This is the improved directory of simulating the heat simulation using PINNS.

The whole PINN was refactored. Other sampling methods were used to sample the data. It was realized that the previous method didn't produced the wished data but only sampled constant values for the data. Also using ReLU proved itself to not be an optimal activation function as it is not twice differentiable. To this end the $\tanh$ activation function was used. 

Increasing the network size improved performance. 

Depending on the training boundary conditions are not kept. 


![heat](Figure_1.png)