import torch
import torch.optim as optim
import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
import matplotlib.animation as animation








class HeatEquationNN_Base(nn.Module):
    def __init__(self,
                 x_min = -0.5, x_max = 0.5,
                 y_min = -0.5, y_max = 0.5,
                 t_min =    0, t_max = 10,
                 Nx = 500, Ny = 500, Nt =500,
                 alpha = 0.1):
        super().__init__()


        # Initialize all the parameters.
        self.x_min, self.x_max = x_min, x_max
        self.y_min, self.y_max = y_min, y_max
        self.t_min, self.t_max = t_min, t_max
        self.Nx, self.Ny, self.Nt = Nx, Ny, Nt
        self.alpha = alpha

        self.xyt, self.boundary, self.init = self.create_data()

    
    def create_data(self):
        
        dx, dy, dt = (self.x_max - self.x_min)/self.Nx, (self.y_max - self.y_min)/self.Ny,  (self.t_max - self.t_min)/self.Nt     


        print("Initialializing Random Data")
        xyt = torch.hstack((torch.linspace(self.x_min + dx, self.x_max - dx, self.Nx).squeeze().view(-1, 1),
                            torch.linspace(self.y_min + dy, self.y_max - dy, self.Ny).squeeze().view(-1, 1), 
                            torch.linspace(self.t_min + dt, self.t_max     , self.Nt).squeeze().view(-1, 1))).requires_grad_(True)

        print("Imposing Boundary Conditions")
        #[-0.5,y,t]
        left_boundary   = torch.hstack((torch.zeros(self.Nx,requires_grad=True).squeeze().view(-1,1) - 0.5, 
                                        torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                                        torch.linspace(self.t_min, self.t_max, self.Nt).squeeze().view(-1, 1))).requires_grad_(True)
        #[0.5, y, t]
        right_boundary  = torch.hstack((torch.zeros(self.Nx,requires_grad=True).squeeze().view(-1,1) + 0.5, 
                                        torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                                        torch.linspace(self.t_min, self.t_max, self.Nt).squeeze().view(-1, 1))).requires_grad_(True)
        #[x, -0.5, t]
        bottom_boundary = torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1), 
                                        torch.zeros(self.Ny,requires_grad=True).squeeze().view(-1,1) - 0.5, 
                                        torch.linspace(self.t_min, self.t_max, self.Nt).squeeze().view(-1, 1))).requires_grad_(True)
        
        #[x, 0.5, t]
        top_boundary    = torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1), 
                                        torch.zeros(self.Ny,requires_grad=True).squeeze().view(-1,1) + 0.5, 
                                        torch.linspace(self.t_min, self.t_max, self.Nt).squeeze().view(-1, 1))).requires_grad_(True)
        boundary = torch.vstack((left_boundary,right_boundary,top_boundary,bottom_boundary))

        print("Imposing Initial Conditions")

        #[-0.5, y, 0]
        left_init =  torch.hstack((torch.zeros(self.Nx,requires_grad=True).squeeze().view(-1,1) - 0.5, 
                                   torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                                   torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1))).requires_grad_(True)
        #[0.5,y,0]
        right_init = torch.hstack((torch.zeros(self.Nx,requires_grad=True).squeeze().view(-1,1) + 0.5, 
                                   torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                                   torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1))).requires_grad_(True)
        
        #[x, 0.5, 0]
        top_init =   torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1), 
                                   torch.zeros(self.Ny,requires_grad=True).squeeze().view(-1,1) + 0.5, 
                                   torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1))).requires_grad_(True)
        
        #[x, -0.5, 0]
        bottom_init = torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1), 
                                    torch.zeros(self.Ny,requires_grad=True).squeeze().view(-1,1)  - 0.5, 
                                    torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1))).requires_grad_(True)
        #[x, 0.5, 0]
        middle_init = torch.hstack((torch.linspace(self.x_min + dx, self.x_max - dx, self.Nx).squeeze().view(-1, 1),
                                    torch.linspace(self.y_min + dy, self.y_max - dy, self.Ny).squeeze().view(-1, 1), 
                                    torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1))).requires_grad_(True)
        
        init  = torch.vstack((left_init,right_init,top_init,bottom_init,middle_init))

        perm_xyt = torch.randperm(xyt.shape[0])
        perm_boundary = torch.randperm(boundary.shape[0])
        perm_init = torch.randperm(init.shape[0])

        return xyt[perm_xyt], boundary[perm_boundary], init[perm_init]


    def visualize_heatmap(self, save_as_gif = False):

        xyt = torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1),
                            torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                            torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1)))
        heat = self.forward(xyt).cpu().detach().numpy()
        

        fig, ax = plt.subplots()
        cax = ax.imshow(heat.T, cmap='jet', origin='lower',
                        extent=[self.x_min,self.x_max,self.y_min,self.y_max], 
                        vmin=0, vmax=1)
        cb = fig.colorbar(cax)
        
        def update(frame):
            
            
            dt = self.t_max/self.Nt * frame

            xyt = torch.hstack((torch.linspace(self.x_min, self.x_max, self.Nx).squeeze().view(-1, 1),
                            torch.linspace(self.y_min, self.y_max, self.Ny).squeeze().view(-1, 1), 
                            torch.zeros(self.Nt,requires_grad=True).squeeze().view(-1, 1) + dt))
            heat = self.forward(xyt).cpu().detach().numpy()


            title = ax.set_title(f"Heat Map at t={dt}")
        
            cax.set_array(heat.T)
            return cax, title
    
        ani = animation.FuncAnimation(fig, update, 
                                      frames=range(0, self.Nt), 
                                      interval=100, blit=False)
        plt.show()
        if save_as_gif: plt.savefig("HeatMap.gif",dpi=300)








    def plot_loss(self,loss_values, epoch_values):
        plt.plot(epoch_values, loss_values)
        plt.title('Loss vs. Epochs')
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.savefig("Loss.png", dpi=300, bbox_inches='tight')








class HeatEquationNN_V1(HeatEquationNN_Base):
    def __init__(self):
        super().__init__()



         
        self.layers = nn.Sequential(
                nn.Linear(3,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,500),
                nn.Tanh(),
                nn.Linear(500,1))
        
        self.optimizer = torch.optim.LBFGS( self.parameters(),
                                            lr=1e-3,
                                            line_search_fn="strong_wolfe")
        

        self.loss_fct = torch.nn.MSELoss()

  
        

   

    def forward(self, x):
        return self.layers(x)
    

    def closure(self):
        #reset gradients
        self.optimizer.zero_grad()
        # loss = l_r + l_data
        #Calculate residual loss
        loss  = self.Loss_residual(self.xyt)
       
        #Calculate data loss on boundary
        left_bndr = self(self.boundary[self.boundary[:,0] == self.x_min])
        other_bndr= self(self.boundary[self.boundary[:,0] != self.x_min])

        left_init = self(self.init[self.init[:,0] == self.x_min])
        other_init =self(self.init[self.init[:,0] != self.x_min])


        loss += self.loss_fct(left_bndr , torch.ones_like(left_bndr))
        loss += self.loss_fct(other_bndr, torch.zeros_like(other_bndr))
        #Calculate data loss for t = 0
        loss += self.loss_fct(left_init,  torch.ones_like(left_init))
        loss += self.loss_fct(other_init, torch.zeros_like(other_init))

      
        
        loss.backward(retain_graph=True)
        return loss
    


    def Loss_residual(self, xyt):
        u_pred = self(xyt)
        grads = torch.autograd.grad(u_pred, xyt, 
                                    grad_outputs=torch.ones_like(u_pred),
                                    retain_graph=True,
                                    create_graph=True)[0]
        
        u_x = grads[:, 0]
        u_y = grads[:, 1]
        u_t = grads[:, 2]

        grads2 = torch.autograd.grad(u_x, xyt,
                                     grad_outputs=torch.ones_like(u_x),
                                     retain_graph=True,
                                     create_graph=True)[0]
        u_xx = grads2[:, 0]
        grads2 = torch.autograd.grad(u_y, xyt,
                                     grad_outputs=torch.ones_like(u_y), 
                                     retain_graph=True,
                                     create_graph=True)[0]
        u_yy = grads2[:, 1]

        
        f = u_t - self.alpha * (u_xx + u_yy)

        return self.loss_fct(f,torch.zeros_like(f))



    def train_model(self,n_epochs=100):
        
        self.train(True)
        loss_arr = []
        epoch_arr = []
        #Training loop
        print("Started Training!")
        for epoch in range(n_epochs):
            loss = self.optimizer.step(self.closure)
            loss_arr.append(loss.item())
            epoch_arr.append(epoch)
            #if epoch % 100 == 0:
            print(f"Epoch {epoch}, Loss: {loss.item()}")

    
    
if __name__ == '__main__':

    device = 'cuda' if torch.cuda.is_available() else 'cpu'

    model = HeatEquationNN_V1()
    model.train_model(n_epochs=50)
    torch.save(model.state_dict(), "model.pt")
    model.visualize_heatmap()

    




    



# self.fc1 = nn.Linear(3, 50)
# self.fc2 = nn.Linear(50, 50)
# self.fc3 = nn.Linear(50, 1)
# self.lambdas = [1, 1, 1, 1, 1]

#   def forward(self, x):
#         x = torch.relu(self.fc1(x))
#         x = torch.relu(self.fc2(x))
#         x = self.fc3(x)
#         return x

        


#     def pinn_lr_annealing(self, L_r, L_i, params, S=1000, eta=1e-3, alpha=0.1):
#         M = len(L_i)
#         lambda_i = torch.ones(M)

#         for n in range(S):
#             lambda_i_hat = torch.zeros(M)
#             grad_L_r = torch.autograd.grad(L_r(params), params, create_graph=True)[0]
#             for i in range(M):
#                 grad_L_i = torch.autograd.grad(L_i[i](params), params, create_graph=True)[0]
#                 lambda_i_hat[i] = torch.max(torch.abs(grad_L_r)) / torch.mean(torch.abs(grad_L_i))

#             lambda_i = (1 - alpha) * lambda_i + alpha * lambda_i_hat

#             grad_L_r = torch.autograd.grad(L_r(params), params)[0]
#             grad_L_i_sum = torch.zeros_like(params)
#             for i in range(M):
#                 grad_L_i = torch.autograd.grad(L_i[i](params), params)[0]
#                 grad_L_i_sum += lambda_i[i] * grad_L_i

#             params = params - eta * (grad_L_r + grad_L_i_sum)

#         return params